<?php

namespace App\Dto;

use Illuminate\Http\Request;

abstract class CreateBaseDto
{
    public abstract function makeRequest(Request $request);
}

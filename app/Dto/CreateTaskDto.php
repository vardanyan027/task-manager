<?php

namespace App\Dto;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class CreateTaskDto extends CreateBaseDto
{
    public string $name;
    public string $status;
    public int $project_id;
    public Carbon $started_at;

    public function makeRequest(Request $request) {
        $this->name = $request->get('name');
        $this->project_id = $request->get('project_id');
        $this->status = $request->get('status') ?? 'started';
        $this->started_at = Carbon::now();
        return $this;
    }
}

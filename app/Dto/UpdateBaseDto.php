<?php

namespace App\Dto;

use Illuminate\Http\Request;

abstract class UpdateBaseDto
{
    public abstract function makeRequest(Request $request);
}

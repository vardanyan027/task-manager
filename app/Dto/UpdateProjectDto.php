<?php

namespace App\Dto;

use Illuminate\Http\Request;

class UpdateProjectDto extends UpdateBaseDto
{
    public string $name;
    public int $timeout;

    public function makeRequest(Request $request) {
        $this->name = $request->get('name');
        $this->timeout = $request->get('timeout');
        return $this;
    }
}

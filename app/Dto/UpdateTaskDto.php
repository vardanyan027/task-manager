<?php

namespace App\Dto;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class UpdateTaskDto extends UpdateBaseDto
{
    public ?string $status;
    public ?int $percent;
    public ?Carbon $finished_at;

    /**
     * @param Request $request
     * @return $this
     */
    public function makeRequest(Request $request): static
    {
        if ($request->has('status')) {
            $this->status = $request->get('status');
            if ($this->status !== 'started') {
                $this->percent = 100;
            }
            $this->finished_at = Carbon::now();
        }
        if ($request->has('percent')) {
            $this->percent = $request->get('percent');
        }
        return $this;
    }
}

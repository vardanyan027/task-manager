<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Services\BaseService;
use App\Dto\CreateBaseDto;
use App\Dto\UpdateBaseDto;

class BaseController extends Controller
{
    public BaseService $service;
    public CreateBaseDto $createDto;
    public UpdateBaseDto $updateDto;

    public function __construct(
        BaseService $service,
        CreateBaseDto $createDto,
        UpdateBaseDto $updateDto
    )
    {
        $this->service = $service;
        $this->createDto = $createDto;
        $this->updateDto = $updateDto;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $items = $this->service->get();
        return response()->json([
            'items' => $items
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $dto = $this->createDto->makeRequest($request);
        $item = $this->service->create($dto);
        return response()->json([
            'item' => $item
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse|JsonResponse
     */
    public function update(Request $request, $id): RedirectResponse|JsonResponse
    {
        $dto = $this->updateDto->makeRequest($request);
        $item = $this->service->update($id, $dto);
        if ($request->has('frontend')) {
            return redirect()->back();
        }
        return response()->json([
            'item' => $item
        ]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->service->delete($id);
            return response()->json([
                'message' => 'Deleted'
            ]);
        } catch (Exception $exception) {
            return response()->json(
                $exception->getMessage(),
                501
            );
        }
    }
}

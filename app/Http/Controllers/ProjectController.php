<?php

namespace App\Http\Controllers;

use App\Services\ProjectService;
use App\Dto\CreateProjectDto;
use App\Dto\UpdateProjectDto;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ProjectController extends BaseController
{
    public function __construct(
        ProjectService $projectService,
        CreateProjectDto $createProjectDto,
        UpdateProjectDto $updateProjectDto
    )
    {
        parent::__construct(
            $projectService,
            $createProjectDto,
            $updateProjectDto
        );
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function get(): \Illuminate\Foundation\Application|View|Factory|Application
    {
        $items = $this->service->get();
        return view('projects.index', ['items' => $items]);
    }
}

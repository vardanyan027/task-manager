<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use App\Dto\CreateTaskDto;
use App\Dto\UpdateTaskDto;
use App\Services\TaskService;

class TaskController extends BaseController
{
    public function __construct(
        TaskService $taskService,
        CreateTaskDto $createTaskDto,
        UpdateTaskDto $updateTaskDto
    )
    {
        parent::__construct(
            $taskService,
            $createTaskDto,
            $updateTaskDto
        );
    }

    /**
     * @param Request $request
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function get(Request $request): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $project_id = $request->get('project_id');
        $items = $this->service->getBy('project_id', $project_id);
        return view('tasks.index', ['items' => $items]);
    }
}

<?php

namespace App\Models;

use App\Dto\CreateProjectDto;
use App\Dto\UpdateProjectDto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';

    protected $fillable = [
        'name',
        'timeout',
        'token'
    ];

    public function create(CreateProjectDto $createProjectDto) {
        $item = new Project();
        $item->name = $createProjectDto->name;
        $item->timeout = $createProjectDto->timeout;
        $item->token = Str::random(16);
        return $item;
    }

    public function updateby(UpdateProjectDto $updateProjectDto) {
        $this->name = $updateProjectDto->name;
        $this->timeout = $updateProjectDto->timeout;
        return $this;
    }

    public function tasks() {
        return $this->hasMany(Task::class);
    }
}

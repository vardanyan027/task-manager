<?php

namespace App\Models;

use App\Dto\CreateTaskDto;
use App\Dto\UpdateTaskDto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = [
        'name',
        'status',
        'project_id',
        'percent',
        'started_at',
        'finished_at',
        'created_at',
        'updated_at'
    ];

    /**
     * @param CreateTaskDto $createTaskDto
     * @return Task
     */
    public function create(CreateTaskDto $createTaskDto): Task
    {
        $item = new Task();
        $item->name = $createTaskDto->name;
        $item->status = $createTaskDto->status;
        $item->project_id = $createTaskDto->project_id;
        $item->started_at = $createTaskDto->started_at;
        return $item;
    }

    /**
     * @param UpdateTaskDto $updateTaskDto
     * @return $this
     */
    public function updateby(UpdateTaskDto $updateTaskDto): static
    {
        if (isset($updateTaskDto->status)) {
            $this->status = $updateTaskDto->status;
            $this->finished_at = $updateTaskDto->finished_at;
        }
        if (isset($updateTaskDto->percent)) {
            $this->percent = $updateTaskDto->percent;
        }
        return $this;
    }

    /**
     * @return BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class);
    }
}

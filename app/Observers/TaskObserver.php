<?php

namespace App\Observers;

use App\Jobs\CheckTasks;
use App\Models\Task;
use Carbon\Carbon;

class TaskObserver
{
    /**
     * Handle the Task "created" event.
     */
    public function created(Task $task): void
    {
        $project = $task->project()->first();
        CheckTasks::dispatch($task)
                    ->delay(now()->addMinutes((int)$project->timeout));
    }
}

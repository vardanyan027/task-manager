<?php

namespace App\Services;

use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class BaseService
{
    public $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function get() {
        return $this->model->get();
    }

    public function create($dto) {
        $item = $this->model->create($dto);
        $item->save();
        return $item;
    }

    public function update($id, $dto) {
        $item = $this->model->where('id', $id)->first();
        $item = $item->updateby($dto);
        $item->save();
        return $item;
    }

    public function delete($id) {
        $item = $this->model->findOrFail($id);
        $item->delete(); 
   }

    public function getBy($key, $value) {
        $items = $this->model->where($key, $value)->get();
        return $items;
    }
}

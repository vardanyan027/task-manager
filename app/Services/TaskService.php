<?php

namespace App\Services;

use App\Dto\CreateTaskDto;
use App\Dto\UpdateTaskDto;
use App\Models\Task;

class TaskService extends BaseService
{
    public function __construct(
        Task $task
    )
    {
        parent::__construct(
            $task
        );
    }
}

@extends('welcome')

@section('table')
<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">TimeOut</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
                <tr>
                    <td scope="row">{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->timeout}}</td>
                    <td>
                        <a href="{{route('tasks', ['project_id' => $item->id])}}">
                            <button>Go</button>
                        </a>
                    </td>
                </tr>
        @endforeach
    </tbody>
</table>
@endsection

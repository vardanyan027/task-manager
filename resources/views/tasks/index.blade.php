@extends('welcome')

@section('table')
<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Started At</th>
            <th>Finished At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
            <tr>
                <td scope="col">{{$item->name}}</td>
                <td scope="col">{{$item->status}}</td>
                <td scope="col">{{$item->started_at}}</td>
                <td scope="col">{{$item->finished_at}}</td>
                <td scope="col">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal-{{$item->id}}">
                    Edit
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal-{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="{{route('tasks.update', ['id' => $item->id, 'task' => $item])}}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <input type="hidden" name="frontend" value="true">
                                <div class="modal-body">
                                    <select id="status" name="status" class="form-control">
                                        <option value="failed" {{ $item->status=="failed" ? "selected" : ''}}>Failed</option>
                                        <option value="finished" {{ $item->status=="finished" ? "selected" : ''}}>Finished</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
